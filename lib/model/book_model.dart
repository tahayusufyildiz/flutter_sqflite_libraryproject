class Book {
  final int? id;
  final String bookName;
  final String author;

  const Book({required this.bookName, required this.author, this.id});

  factory Book.fromJson(Map<String, dynamic> json) => Book(
        id: json['id'],
        bookName: json['bookName'],
        author: json['author'],
      );

  Map<String, dynamic> toJson() => {
        'id': id,
        'bookName': bookName,
        'author': author,
      };
}
