import 'package:flutter/material.dart';
import 'package:flutter_sqflite_example/view/home_view.dart';
import 'package:get/get.dart';

import '../model/book_model.dart';
import '../service/database_helper.dart';

class Editing extends StatelessWidget {
  final Book? book;
  const Editing({
    Key? key,
    this.book,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final bookNameController = TextEditingController();
    final authorController = TextEditingController();

    if (book != null) {
      bookNameController.text = book!.bookName;
      authorController.text = book!.author;
    }
    return Scaffold(
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      appBar: AppBar(
        backgroundColor: Colors.orange.shade300,
        title: Text(
          book == null ? "Add a new book" : "Editing",
          style: TextStyle(fontWeight: FontWeight.bold),
        ),
      ),
      floatingActionButton: FloatingActionButton.extended(
        onPressed: () async {
          final bookname = bookNameController.value.text;
          final author = authorController.value.text;

          if (bookname.isEmpty || author.isEmpty) {
            return;
          }

          final Book model =
              Book(bookName: bookname, author: author, id: book?.id);
          if (book == null) {
            await DatabaseHelper.addBook(model);
          } else {
            await DatabaseHelper.updateBook(model);
          }
          Get.to(Home());
        },
        label: Text(
          book == null ? "SAVE" : "EDIT",
          style: TextStyle(fontSize: 19, fontWeight: FontWeight.bold),
        ),
        backgroundColor: Colors.orange.shade300,
      ),
      body: Padding(
        padding: const EdgeInsets.all(15.0),
        child: Column(children: [
          TextFormField(
            controller: bookNameController,
            decoration: const InputDecoration(
              hintText: "Book Name",
              labelText: "Book Name",
              border: OutlineInputBorder(
                  borderSide: BorderSide(
                    width: 1,
                    color: Colors.orange,
                  ),
                  borderRadius: BorderRadius.all(Radius.circular(12))),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 8),
            child: TextFormField(
              controller: authorController,
              decoration: const InputDecoration(
                hintText: "Author",
                labelText: "Author Name",
                border: OutlineInputBorder(
                    borderSide: BorderSide(
                      width: 1,
                      color: Colors.orange,
                    ),
                    borderRadius: BorderRadius.all(Radius.circular(12))),
              ),
              keyboardType: TextInputType.multiline,
            ),
          ),
        ]),
      ),
    );
  }
}
