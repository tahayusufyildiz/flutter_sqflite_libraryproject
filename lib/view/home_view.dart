import 'package:flutter/material.dart';
import 'package:flutter_sqflite_example/view/adding_view.dart';
import 'package:get/get.dart';

import '../component/book_widget.dart';
import '../model/book_model.dart';
import '../service/database_helper.dart';

class Home extends StatefulWidget {
  const Home({super.key});

  @override
  State<Home> createState() => _HomeState();
}

class _HomeState extends State<Home> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
        appBar: AppBar(
          backgroundColor: Colors.orange.shade300,
          title: const Text(
            "LIBRARY",
            style: TextStyle(fontWeight: FontWeight.bold),
          ),
        ),
        floatingActionButton: FloatingActionButton.extended(
          backgroundColor: Colors.orange.shade300,
          icon: const Icon(
            Icons.add,
            size: 22,
          ),
          label: const Text(
            "Add a Book",
            style: TextStyle(fontWeight: FontWeight.bold, fontSize: 19),
          ),
          onPressed: () {
            Get.to(Editing());
          },
        ),
        body: FutureBuilder<List<Book>?>(
          future: DatabaseHelper.getAllBook(),
          builder: (context, AsyncSnapshot<List<Book>?> snapshot) {
            if (snapshot.connectionState == ConnectionState.waiting) {
              return const CircularProgressIndicator();
            } else if (snapshot.hasError) {
              return Center(child: Text(snapshot.error.toString()));
            } else if (snapshot.hasData) {
              if (snapshot.data != null) {
                return ListView.builder(
                  itemBuilder: (context, index) => BookWidget(
                    book: snapshot.data![index],
                    onTap: () async {
                      await Get.to(Editing(
                        book: snapshot.data![index],
                      ));
                      setState(() {});
                    },
                    onLongPress: () async {
                      showDialog(
                          context: context,
                          builder: (context) {
                            return AlertDialog(
                              title: const Text('Delete ?'),
                              actions: [
                                ElevatedButton(
                                  style: ButtonStyle(
                                      backgroundColor:
                                          MaterialStateProperty.all(
                                              Colors.red)),
                                  onPressed: () async {
                                    await DatabaseHelper.deleteBook(
                                        snapshot.data![index]);
                                    Get.back();
                                    setState(() {});
                                  },
                                  child: const Text('Yes'),
                                ),
                                ElevatedButton(
                                  style: ButtonStyle(
                                      backgroundColor:
                                          MaterialStateProperty.all(
                                              Colors.orange.shade300)),
                                  onPressed: () => Get.back(),
                                  child: const Text('No'),
                                ),
                              ],
                            );
                          });
                    },
                  ),
                  itemCount: snapshot.data!.length,
                );
              }
              return const Center(
                child: Text('No books yet'),
              );
            }
            return const SizedBox.shrink();
          },
        ));
  }
}
