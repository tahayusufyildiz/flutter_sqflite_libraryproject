import 'package:flutter_sqflite_example/model/book_model.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';

class DatabaseHelper {
  static const int _version = 1;
  static const String _dbName = "_Book.db";

  //this for opening the DB . We will use all operations.
  static Future<Database> _getDb() async {
    return openDatabase(
      join(await getDatabasesPath(), _dbName),
      onCreate: (db, version) async => await db.execute(
          "CREATE TABLE Book(id INTEGER PRIMARY KEY, bookName TEXT NOT NULL, author TEXT NOT NULL);"),
      version: _version,
    );
  }

// this future for adding a book
  static Future<int> addBook(Book book) async {
    final db = await _getDb();
    return await db.insert("Book", book.toJson(),
        conflictAlgorithm: ConflictAlgorithm.replace);
  }

// this future for updating a book
  static Future<int> updateBook(Book book) async {
    final db = await _getDb();
    return await db.update(
      "Book",
      book.toJson(),
      where: 'id = ?',
      whereArgs: [book.id],
      conflictAlgorithm: ConflictAlgorithm.replace,
    );
  }

//this future to delete a book
  static Future<int> deleteBook(Book book) async {
    final db = await _getDb();
    return await db.delete(
      "Book",
      where: 'id = ?',
      whereArgs: [book.id],
    );
  }

  //This is to get all data on DB and save a list
  static Future<List<Book>?> getAllBook() async {
    final db = await _getDb();
    final List<Map<String, dynamic>> maps = await db.query("Book");

    if (maps.isEmpty) {
      return null;
    }
    return List.generate(maps.length, (index) => Book.fromJson(maps[index]));
  }
}
